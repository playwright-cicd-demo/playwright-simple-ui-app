### Build the Source Code
FROM node:10-alpine as builder

WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm ci

COPY src ./src
COPY public ./public

RUN npm run builder

### Run the Distributable

FROM node:10-alpine

WORKDIR /usr/dist/app

COPY --from=builder /usr/src/app/build ./
COPY env.sh .env ./
RUN chmod +x env.sh

RUN npm install -g serve
EXPOSE 8080
ENTRYPOINT ./env.sh . && serve -s . -l 8080