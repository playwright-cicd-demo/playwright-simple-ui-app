function getEnvironmentVariable(env) {
    const value = window.env[env];
    if (!value) {
        throw new Error(`Missing env for ${env}`);
    }
    return value;
}

export const API_URL = getEnvironmentVariable("API_URL");