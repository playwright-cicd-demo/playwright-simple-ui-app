#!/bin/sh

ENV_FILE=.env
OUTPUT_DIR=$1
OUTPUT_FILE=${OUTPUT_DIR}/env-config.js

# Ensure directory exists
mkdir -p ${OUTPUT_DIR}

# Recreate config file
rm -rf ${OUTPUT_FILE}
touch ${OUTPUT_FILE}

# Add assignment
echo "window.env = {" >> ${OUTPUT_FILE}

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [ -n "$line" ];
do
  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varName=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varDefaultValue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  varEnvironmentValue=$(eval echo \$$varName)

  if [ ! -z "${varEnvironmentValue}" ]; then
    # Read value of current variable if exists as Environment variable
    value=$(printf '%s\n' "${varEnvironmentValue}")
  else
    # Otherwise use value from .env file
    value=${varDefaultValue}
  fi

  if [ -z "${value}" ]; then
    echo "envionment variable required for \"${varName}\""
    exit 1
  fi

  # Append configuration property to JS file
  echo "  ${varName}: \"${value}\"," >> ${OUTPUT_FILE}

done < ${ENV_FILE}

echo "}" >> ${OUTPUT_FILE}
